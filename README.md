# Projet WebService SOAP

## Étudiants
 - LOMBARDO Florian
 - REMOND Victor
 - TAGUEJOU Christian

## Notes :
Projet développé sous Linux (Ubuntu) avec IntelliJ.

Fonctionnement testé sous Linux & Windows, avec TomCat9 et Axis2.

Pour utiliser Axis2 avec IntelliJ, aller dans **File** > **Settings** puis taper *Axis* dans la barre de recherche et indiquer le chemin vers le répertoire Axis2 dans le camps pévu à cet effet.

Le WebService se lance avec la configuration *server*, le client avec la configuration *client*.

Enjoy !
