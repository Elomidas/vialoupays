<%@ page import="client.PaysServiceStub" %><%--
  Created by IntelliJ IDEA.
  User: elomidas
  Date: 07/10/18
  Time: 12:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Details</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">Client Pays</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="/">Home</a></li>
            <li><a href="/list">Liste des pays</a></li>
        </ul>
    </div>
</nav>
<div class="container">
    <%
        PaysServiceStub.Pays p = (PaysServiceStub.Pays)request.getAttribute("pays");
    %>
    <div class="row">
        <h1>Informations</h1>
    </div>
    <div class="row">
        <h2>Détails</h2>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2">
            <table class="table table-hover" style="text-align: center">
                <tr>
                    <td>Pays</td>
                    <td><%=p.getNomPays()%></td>
                </tr>
                <tr>
                    <td>Capitale</td>
                    <td><%=p.getNomCapitale()%></td>
                </tr>
                <tr>
                    <td>Continent</td>
                    <td><%=p.getNomContinent()%></td>
                </tr>
                <tr>
                    <td>Population</td>
                    <td><%=p.getNbHabitants()%> habitants</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row">
        <h2>Carte</h2>
    </div>
    <div class="row">
        <div class="col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3">
            <iframe width="100%" height="50%" frameborder="0"
                    scrolling="no" marginheight="0" marginwidth="0"
                    src="https://maps.google.com/maps?hl=fr&amp;q=<%=p.getNomPays()%>&amp;output=embed">
            </iframe>
        </div>
    </div>
</body>
</html>
