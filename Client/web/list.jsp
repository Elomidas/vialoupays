<%@ page import="java.util.List" %>
<%@ page import="client.PaysServiceStub" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: elomidas
  Date: 07/10/18
  Time: 12:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>List</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="/">Client Pays</a>
            </div>
            <ul class="nav navbar-nav">
                <li><a href="/">Home</a></li>
                <li class="active"><a href="#">Liste des pays</a></li>
            </ul>
        </div>
    </nav>
    <div class="container">
        <h1>Liste des pays</h1>
        <table class="table table-hover">
            <thead>
                <th>Pays</th>
                <th>Continent</th>
                <th>Description</th>
            </thead>
            <tbody>
            <%
                List<PaysServiceStub.Pays> list = (ArrayList<PaysServiceStub.Pays>)request.getAttribute("paysList");
                for (PaysServiceStub.Pays p : list) {
                    %>
                    <tr>
                        <td><%=p.getNomPays()%></td>
                        <td><%=p.getNomContinent()%></td>
                        <td>
                            <a href="details?pays=<%=p.getNomPays()%>">
                                <span class="glyphicon glyphicon-search">
                                </span>
                            </a>
                        </td>
                    </tr>
                    <%
                }
            %>
            </tbody>
        </table>
    </div>
</body>
</html>
