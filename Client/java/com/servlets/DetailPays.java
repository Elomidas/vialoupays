package com.servlets;

import client.PaysServiceMonExceptionException;
import client.PaysServiceStub;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DetailPays extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        PaysServiceStub service = new PaysServiceStub();
        PaysServiceStub.ConsulterPays monPays = new PaysServiceStub.ConsulterPays();
        PaysServiceStub.ConsulterPaysResponse paysResponse;
        PaysServiceStub.Pays result = new PaysServiceStub.Pays();
        String param = request.getParameter("pays");
        if(param != null) {
            try {
                monPays.setNom(param);
                paysResponse = service.consulterPays(monPays);
                PaysServiceStub.Pays tmp = paysResponse.get_return();
                result = tmp;
            } catch (PaysServiceMonExceptionException e) {
                e.printStackTrace();
            }
            request.setAttribute("pays", result);
            this.getServletContext().getRequestDispatcher("/details.jsp").forward(request, response);
        } else {
            this.getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
        }

    }
}
