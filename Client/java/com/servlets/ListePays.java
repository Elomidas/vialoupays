package com.servlets;

import client.PaysServiceMonExceptionException;
import client.PaysServiceStub;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListePays extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        PaysServiceStub service = new PaysServiceStub();
        PaysServiceStub.ConsulterListePays mesPays = new PaysServiceStub.ConsulterListePays();
        PaysServiceStub.ConsulterListePaysResponse listePaysResponse;
        List<PaysServiceStub.Pays> paysList = new ArrayList<PaysServiceStub.Pays>();
        try {
            listePaysResponse = service.consulterListePays(mesPays);
            PaysServiceStub.Pays[] tabPays = listePaysResponse.get_return();
            Collections.addAll(paysList, tabPays);
        } catch (PaysServiceMonExceptionException e) {
            e.printStackTrace();
        }
        request.setAttribute("paysList", paysList);
        this.getServletContext().getRequestDispatcher( "/list.jsp" ).forward( request, response );
    }
}
